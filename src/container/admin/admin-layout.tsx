/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import "./admin.css"
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';

const AdminLayout = () => {
    let name = localStorage.getItem("credentials-username")
    console.log(name, "name")
    return (
        <>
            <div className="todo">
                <nav>
                    <div className="logo">
                        <div><img src={require('../../assets/logo/Logo Transparent 5.png')} alt="" /></div>
                    </div>
                    <ul>
                        <li>Dashboard</li>
                        <li>Components</li>
                        <li>Widgets<i className="fas fa-chevron-right flecha"></i>
                            <ul className="subboton">
                                <li>Desk</li>
                            </ul>
                        </li>
                        <li>Metrics</li>
                        <li>Tables<i className="fas fa-chevron-right flecha"></i>
                            <ul className="subboton">
                                <li>Sales</li>
                            </ul>
                        </li>
                        <li>Timeline</li>
                    </ul>
                </nav>

                <main className="panelD">
                    <header>

                        {/* ruby roy */}
                        <div style={{
                            display: "flex",
                            justifyContent: "end",
                            alignItems: "center",
                            padding: "15px"
                        }}>
                            <PopupState variant="popover" popupId="demo-popup-menu">
                                {(popupState) => (
                                    <React.Fragment>
                                        <Button variant="contained" {...bindTrigger(popupState)}>
                                            Hello {name}
                                        </Button>
                                        <Menu {...bindMenu(popupState)}>
                                            <MenuItem onClick={popupState.close}>Profile</MenuItem>
                                            <MenuItem onClick={popupState.close}>My account</MenuItem>
                                            <MenuItem onClick={popupState.close}>Logout</MenuItem>
                                        </Menu>
                                    </React.Fragment>
                                )}
                            </PopupState>
                        </div>
                    </header>

                    <h1 className="tituloD">Dashboard</h1>

                    <h2>Ventas de los ultimos meses<i className="fas fa-arrow-down"></i></h2>
                    <div className="tabla">
                        <table cellPadding="0" cellSpacing="0">
                            <tr className="negrita">
                                <th>&nbsp;</th>
                                <th>Remeras</th>
                                <th>Pantalones</th>
                                <th>Buzos</th>
                            </tr>
                            <tr>
                                <th>Enero</th>
                                <th>$300</th>
                                <th>$758</th>
                                <th>$314</th>
                            </tr>
                            <tr>
                                <th>Febrero</th>
                                <th>$453</th>
                                <th>$159</th>
                                <th>$239</th>
                            </tr>
                        </table>
                    </div>

                    <div className="formularios">
                        <form className="" action="#" method="post">
                            <h3>Fecha:</h3>
                            <input type="text" name="mes" value="" placeholder="Mes..." />
                            <br />
                            <h3>Ingresos:</h3>
                            <input type="text" name="remeras" value="" placeholder="Remeras..." />
                            <input type="text" name="pantalones" value="" placeholder="Pantalones..." />
                            <input type="text" name="buzos" value="" placeholder="Buzos..." />
                            <br />
                            <input type="submit" name="enviar" value="Enviar" className="botonEnviar" />
                        </form>
                    </div>

                    <h2>Ultimas 5 ventas<i className="fas fa-arrow-down"></i></h2>
                    <div className="tabla">
                        <table cellPadding="0" cellSpacing="0">
                            <tr className="negrita">
                                <th>Producto</th>
                                <th>Monto</th>
                            </tr>
                            <tr>
                                <th>Remera adidas</th>
                                <th>$300</th>
                            </tr>
                            <tr>
                                <th>Jean azul</th>
                                <th>$453</th>
                            </tr>
                            <tr>
                                <th>Remera cuadros</th>
                                <th>$153</th>
                            </tr>
                            <tr>
                                <th>Jean negro</th>
                                <th>$450</th>
                            </tr>
                            <tr>
                                <th>Zapatillas nike</th>
                                <th>$890</th>
                            </tr>
                        </table>
                    </div>



                </main>
            </div>
        </>
    )
}

export default AdminLayout