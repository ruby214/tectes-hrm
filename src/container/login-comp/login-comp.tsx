/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from 'react'
import "./login-comp.css"
import { useNavigate } from 'react-router';
import { FaEye, FaEyeSlash } from 'react-icons/fa';
import { Link } from 'react-router-dom';


interface LoginCredentials {
    username: string;
    password: string;
}

export const LoginComp = () => {

    const [credentials, setCredentials] = useState<LoginCredentials>({
        username: '',
        password: '',
    });

    const [showPassword, setShowPassword] = useState<boolean>(false);

    const togglePasswordVisibility = () => {
        setShowPassword((prevShowPassword) => !prevShowPassword);
    };

    const navigate = useNavigate()

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setCredentials((prevCredentials: any) => ({
            ...prevCredentials,
            [name]: value,
        }));

    };

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        if (credentials.username === 'admin' && credentials.password === 'admin') {
            navigate('/admin');
        } else {
            navigate('/user');
        }
        localStorage.setItem("credentials-username", credentials.username)
        // Perform login logic here using the credentials
        console.log(credentials);
    };

    return (
        <div>
            <div className="wrapper">
                <div className="container">
                    <div className="col-left">
                        <div className="login-text">
                            <h2>
                                <img src={require('../../assets/logo/Logo Transparent 3.png')} alt="" />
                            </h2>
                            <p>
                                <b><i>Intelligent Technology For A Seamless Digital Journey</i></b>
                                <br />
                                We built potential mobile computing through applications for your business with enhanced security. Be at the first phase of the fierce competition with our expertise.
                            </p>
                            <a className="btn-1" href="">
                                <Link to="http://www.tectes.in/" target="_blank" className='link_btn'>Read More</Link>
                            </a>
                        </div>
                    </div>
                    <div className="col-right">
                        <div className="login-form">
                            <h2>Login</h2>
                            <form onSubmit={handleSubmit}>
                                <div style={{
                                    display: "flex",
                                    justifyContent: "center",
                                    flexDirection: "column",
                                    gap: "20px"
                                }}>
                                    <p>
                                        <input type="text" placeholder="Username" name="username"
                                            value={credentials.username}
                                            onChange={handleChange} required />
                                    </p>
                                    {/* <p className="form__input">
                                        <input type="password" placeholder="Password" name="password"
                                            value={credentials.password}
                                            onChange={handleChange} required />
                                        <span className="fa fa-eye"></span>
                                    </p> */}
                                    <div className="password-input">
                                        <div className="input-container">
                                            <input
                                                name="password"
                                                type={showPassword ? 'text' : 'password'}
                                                value={credentials.password}
                                                onChange={handleChange}
                                                placeholder="Password"
                                            />
                                            <button
                                                type="button"
                                                className="toggle-password-button"
                                                onClick={togglePasswordVisibility}
                                            >
                                                {showPassword ? <FaEyeSlash size={23} /> : <FaEye size={23} />}
                                            </button>
                                        </div>
                                    </div>
                                    <p>
                                        <input className="btn" type="submit" value="Sign In" />
                                    </p>
                                    <div style={{
                                        display: "flex",
                                        justifyContent: "space-between",
                                        flexDirection: "row",

                                    }}>
                                        <a href="" style={{
                                            textDecoration: "none",
                                            color: "#666666"
                                        }}>Forget password?</a>
                                        <a href="" style={{
                                            textDecoration: "none",
                                            color: "#666666"
                                        }}>Create an account.</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
