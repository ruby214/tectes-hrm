import React from 'react'
import { Routes, Route } from "react-router-dom"
import Admin from '../container/admin/admin'
import User from '../container/user/user'
import { LoginComp } from '../container/login-comp/login-comp'
import AdminLayout from '../container/admin/admin-layout'

const AllRoutes = () => {
    return (
        <>
            <Routes>

                <Route path="/admin" element={<AdminLayout />} />
                <Route path="/user" element={<User />} />
                <Route path="/login" element={<LoginComp />} />
            </Routes>
        </>
    )
}

export default AllRoutes