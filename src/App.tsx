import React from 'react';
import './App.css';
import { LoginComp } from './container/login-comp/login-comp';
import AllRoutes from './routes/all-routes';

function App() {
  return (
    <div className="App">
      <AllRoutes />
    </div>
  );
}

export default App;
